using Microsoft.VisualStudio.TestTools.UnitTesting;
using Compte1;
namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDebiter()
        {
          
            Compte c = new Compte(12347, "Toto", 1200, -500);
            double montant = 2000;
            bool ok;
            ok = c.Debiter(montant);
            Assert.AreEqual(c.Solde,1200);

        }
        
        [TestMethod]
        public void TestSuperieur()
        {
            Compte c = new Compte(46748, "Tutu", 1600, -600);
            Compte c2 = new Compte(24658, "Titi", 1250, -400);
            bool ok;
            ok = c.superieur(c2);
            Assert.AreEqual(ok, true);
            
        }
        [TestMethod]
        public void TestTransferer()
        {
            Compte c = new Compte(79687, "Tata", 1470, -700);
            Compte c2 = new Compte(36446, "Riri", 2510, -300);
            bool ok;
            double  montant = 1000;
            ok = c.Transferer(montant, c2);
            Assert.AreEqual(ok, true);
            Assert.AreEqual(c.Solde, 470);
            Assert.AreEqual(c2.Solde, 3510);

        }
    }
}
